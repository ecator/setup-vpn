# 概览

[![pipeline status](https://gitlab.com/ecator/setup-vpn/badges/master/pipeline.svg)](https://gitlab.com/ecator/setup-vpn/commits/master)

部署vpn的脚本，目前只支持CentOS 7系列，主要参考了[setup-ipsec-vpn/vpnsetup_centos.sh](https://github.com/hwdsl2/setup-ipsec-vpn/blob/master/vpnsetup_centos.sh)以及[CentOS7 搭建L2TP](https://www.linuxprobe.com/centos7-install-l2tp.html)来实现自动化安装。

# 一些设置

为了简化安装，初始化用户和密码非常简单，建议安装完毕后自行修改以下文件：

- 共享密钥 `/etc/ipsec.secrets`
- 用户以及对应IP `/etc/ppp/chap-secrets`
- 客户端分配的IP段 `/etc/xl2tpd/xl2tpd.conf - [local ip] [ip range]`

修改完成后可以用下面的命令重启服务：

```
systemctl restart xl2tpd ipsec
```

# 连接

## 树莓派

下面操作在树莓派中实验成功，理论上Debain系列的都可以。

### 安装依赖

```
install -y strongswan xl2tpd
```

> 因为`openswan`已经过时，所以安装`strongswan`来提供ipsec。

### 配置

**/etc/ipsec.conf**

```
config setup

conn %default
  ikelifetime=60m
  keylife=20m
  rekeymargin=3m
  keyingtries=1
  keyexchange=ikev1
  authby=secret
  ike=aes128-sha1-modp1024,3des-sha1-modp1024!
  esp=aes128-sha1-modp1024,3des-sha1-modp1024!
conn <连接名称>
  keyexchange=ikev1
  left=%defaultroute
  auto=add
  authby=secret
  type=transport
  leftprotoport=17/1701
  rightprotoport=17/1701
  right=<VPN的地址或者域名>
```

当`right`后面跟域名的时候需要把服务器上的同一文件的`leftid`属性改成`@域名`的形式。


**/etc/ipsec.secrets**
```
: PSK "<共享密钥>"
```

**/etc/xl2tpd/xl2tpd.conf**
```
[lac <连接名称>]
lns = <VPN的地址或者域名>
ppp debug = yes
pppoptfile = /etc/ppp/options.l2tpd.client
length bit = yes
```
**/etc/ppp/options.l2tpd.client**

```
ipcp-accept-local
ipcp-accept-remote
refuse-eap
require-mschap-v2
noccp
noauth
noipdefault
idle 1800
mtu 1410
mru 1410
defaultroute
usepeerdns
debug
connect-delay 5000
name <用户名>
password <密码>
```

### 连接

以上配置完成后就可以通过下面的命令连接了。

**启动服务**

```
systemctl start ipsec
systemctl start xl2tpd
```

**连接**

```
ipsec up <连接名称>
```
不出意外上面的命令会显示连接结果，如果连接成功就可以通过`echo 'c <连接名称>' >/run/xl2tpd/l2tp-control`来添加`ppp0`接口，顺便会添加一条路由。

如果出现`ipsec up `成功但是服务端报`Peer is not authorized to use remote address xxxx`的错误一般就是`/etc/ppp/options.l2tpd.client`文件中没有添加`noipdefault`参数。

**路由**

可以通过下面的形式添加一条VPN网段的路由：

```
ip route add x.x.x.x/24 dev ppp0
```

**断开连接**

```
echo 'd <连接名称>'> /run/xl2tpd/l2tp-control 
ipsec down <连接名称>
```


## 其他Linux

请参考[使用命令行配置 Linux VPN 客户端](https://github.com/hwdsl2/setup-ipsec-vpn/blob/master/docs/clients-zh.md#%E4%BD%BF%E7%94%A8%E5%91%BD%E4%BB%A4%E8%A1%8C%E9%85%8D%E7%BD%AE-linux-vpn-%E5%AE%A2%E6%88%B7%E7%AB%AF) :smile:

**主要参考**

- [How to Connect to L2TP/IPsec VPN on Linux](https://www.elastichosts.com/blog/linux-l2tpipsec-vpn-client/)
- [connect to meraki client vpn from strongswan (ubuntu 16.04 edition)](https://gist.github.com/psanford/42c550a1a6ad3cb70b13e4aaa94ddb1c)
- [L2tp IPSEC PSK VPN client on (x)ubuntu 16.04 - Ask Ubuntu](https://askubuntu.com/questions/789421/l2tp-ipsec-psk-vpn-client-on-xubuntu-16-04/797764)
- [vpn - How to set up L2TP client on Ubuntu 18.04? - Ask Ubuntu](https://askubuntu.com/questions/1146861/how-to-set-up-l2tp-client-on-ubuntu-18-04)


