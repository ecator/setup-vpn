#!/bin/bash

# 目前只支持CentOS 7


# 客户端分配的IP地址范围
# 对应 /etc/xl2tpd/xl2tpd.conf 的[local ip]和[ip range]
L2TP_LOCAL='192.168.233.1'
L2TP_POOL='192.168.233.100-192.168.233.250'

# 设置 PSK
# 对应 /etc/ipsec.secrets
VPN_IPSEC_PSK='vpn'

# 设置初始用户
# 对应 /etc/ppp/chap-secrets
# 可以按照下面的形式手动添加多个用户
#    "$VPN_USER" l2tpd "$VPN_PASSWORD" "$VPN_IP"
VPN_USER='user1'
VPN_PASSWORD='user1_pw'
VPN_IP='192.168.233.100'


exiterr() {
    echo "Error: $1" >&2
    exit 1
}
conf_bk() { /bin/cp -f "$1" "$1.old-$SYS_DT" 2>/dev/null; }

# 安装依赖
install-dep() {
    yum install -y epel-release
    yum install -y xl2tpd libreswan bind-utils wget
}

# 获取默认网络接口
get-interface() {
    def_iface=$(route 2>/dev/null | grep -m 1 '^default' | grep -o '[^ ]*$')
    [ -z "$def_iface" ] && def_iface=$(ip -4 route list 0/0 2>/dev/null | grep -m 1 -Po '(?<=dev )(\S+)')
    def_state=$(cat "/sys/class/net/$def_iface/operstate" 2>/dev/null)
    if [ -n "$def_state" ] && [ "$def_state" != "down" ]; then
        case "$def_iface" in
        wl*)
            exiterr "Wireless interface '$def_iface' detected. DO NOT run this script on your PC or Mac!"
            ;;
        esac
        NET_IFACE="$def_iface"
    else
        eth0_state=$(cat "/sys/class/net/eth0/operstate" 2>/dev/null)
        if [ -z "$eth0_state" ] || [ "$eth0_state" = "down" ]; then
            exiterr "Could not detect the default network interface."
        fi
        NET_IFACE=eth0
    fi
    echo "NET_IFACE: $NET_IFACE"
}

# 获取公网IP
get-ip() {
    PUBLIC_IP=$(dig @resolver1.opendns.com -t A -4 myip.opendns.com +short)
    if [ -z "$PUBLIC_IP" ]; then
        PUBLIC_IP=$(wget -t 3 -T 15 -qO- http://ipv4.icanhazip.com)
    fi
    if [ -z "$PUBLIC_IP" ]; then
        exiterr "Cannot detect this server's public IP."
    fi
    echo "PUBLIC_IP: $PUBLIC_IP"
}

# 设置ipsec.conf
setup-ipsec-conf() {
    conf_bk "/etc/ipsec.conf"
    cat >/etc/ipsec.conf <<EOF
version 2.0
config setup
  virtual-private=%v4:10.0.0.0/8,%v4:192.168.0.0/16,%v4:172.16.0.0/12
  protostack=netkey
  interfaces=%defaultroute
  uniqueids=no
conn shared
  left=%defaultroute
  leftid=$PUBLIC_IP
  right=%any
  encapsulation=yes
  authby=secret
  pfs=no
  rekey=no
  keyingtries=5
  dpddelay=30
  dpdtimeout=120
  dpdaction=clear
  ikev2=never
  ike=aes256-sha2,aes128-sha2,aes256-sha1,aes128-sha1,aes256-sha2;modp1024,aes128-sha1;modp1024
  phase2alg=aes_gcm-null,aes128-sha1,aes256-sha1,aes256-sha2_512,aes128-sha2,aes256-sha2
  sha2-truncbug=no
conn l2tp-psk
  auto=add
  leftprotoport=17/1701
  rightprotoport=17/%any
  type=transport
  phase2=esp
  also=shared
EOF
}

# 设置 PSK
setup-ipsec-secrets() {
    conf_bk "/etc/ipsec.secrets"
    cat >/etc/ipsec.secrets <<EOF
%any  %any  : PSK "$VPN_IPSEC_PSK"
EOF
}

# 设置 xl2tpd
setup-xl2tpd() {
    # Create xl2tpd config
    conf_bk "/etc/xl2tpd/xl2tpd.conf"
    cat >/etc/xl2tpd/xl2tpd.conf <<EOF
[global]
port = 1701
[lns default]
ip range = $L2TP_POOL
local ip = $L2TP_LOCAL
require chap = yes
refuse pap = yes
require authentication = yes
name = l2tpd
pppoptfile = /etc/ppp/options.xl2tpd
length bit = yes
EOF

    # Set xl2tpd options
    conf_bk "/etc/ppp/options.xl2tpd"
    cat >/etc/ppp/options.xl2tpd <<EOF
+mschap-v2
ipcp-accept-local
ipcp-accept-remote
noccp
auth
mtu 1280
mru 1280
proxyarp
lcp-echo-failure 4
lcp-echo-interval 30
connect-delay 5000
ms-dns 8.8.8.8
ms-dns 1.1.1.1
EOF
}

# 设置初始用户
setup-user() {
    conf_bk "/etc/ppp/chap-secrets"
    cat >/etc/ppp/chap-secrets <<EOF
"$VPN_USER" l2tpd "$VPN_PASSWORD" "$VPN_IP"
EOF
}

# 设置sysctl
setup-sysctl() {
    cat >/etc/sysctl.d/99-ipsec.conf <<EOF
kernel.msgmnb = 65536
kernel.msgmax = 65536
net.ipv4.ip_forward = 1
net.ipv4.conf.all.accept_source_route = 0
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.all.rp_filter = 0
net.ipv4.conf.default.accept_source_route = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.default.send_redirects = 0
net.ipv4.conf.default.rp_filter = 0
net.ipv4.conf.$NET_IFACE.send_redirects = 0
net.ipv4.conf.$NET_IFACE.rp_filter = 0
net.core.wmem_max = 12582912
net.core.rmem_max = 12582912
net.ipv4.tcp_rmem = 10240 87380 12582912
net.ipv4.tcp_wmem = 10240 87380 12582912
EOF
    sysctl --system
}

# 安装vpn
setup-vpn() {
    install-dep
    get-interface
    get-ip
    setup-ipsec-conf
    setup-ipsec-secrets
    setup-xl2tpd
    setup-user
    setup-sysctl

    # 设置防火墙
    echo "add-port 1701/udp"
    firewall-cmd --permanent --add-port 1701/udp
    echo "add-service ipsec"
    firewall-cmd --permanent --add-service ipsec
    # 如果不想通过vpn上网可以注释掉
    echo "add-masquerade"
    firewall-cmd --permanent --add-masquerade
    firewall-cmd --reload

    # 启动服务
    systemctl enable xl2tpd
    systemctl start xl2tpd
    systemctl enable ipsec
    systemctl start ipsec
    ipsec verify
    cat <<EOF
--------------------------------
服务启动成功！
请选择 L2TP/IPSec 方式连接
服务器地址：$PUBLIC_IP
默认用户名：$VPN_USER
默认密码：$VPN_PASSWORD
共享密钥：$VPN_IPSEC_PSK
--------------------------------
修改共享密钥请查看 /etc/ipsec.secrets
管理用户请查看 /etc/ppp/chap-secrets
服务重启命令 systemctl restart xl2tpd ipsec

EOF

}

if grep -q 'CentOS.*release 7' /etc/redhat-release; then
    setup-vpn
else
    exiterr "please run on CentOS 7!"
fi
